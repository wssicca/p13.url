<?php

namespace p13\url;

/**
 * Description of Parser
 *
 * @author Wagner Sicca <wagnersicca@ifsul.edu.br>
 * @namespace p13\url
 * @package p13\url
 */
class Parser
{

    /**
     *
     * @var URL;
     */
    public $url;

    public function __construct()
    {
        $this->url = new URL();
    }

    /**
     * Interpreta a URL a partir dos dados presentes em $_SERVER
     * @param array $server
     * @return URL
     */
    public function parseFromServer(array $server)
    {
        $this->setSchemeFromServer($server);
        $this->setHierarchicalFromServer($server);
        $this->setQueryFromServer($server);
        return $this->url;
    }

    /**
     * 
     * @param string $serverName
     */
    private function setHierarchicalAuthority($serverName)
    {
        if (isset($serverName) && !empty($serverName)) {
            /* Se houver arroba, tem userinfo */
            $pos_arroba = strpos($serverName, '@');
            if ($pos_arroba !== false) {
                $this->url->hierarchicalPart->authority->userInfo->setValue(
                        substr($serverName, 0, strpos($serverName, '@'))
                );
            }

            /* Se houver dois pontos, tem porta */
            $pos_2pontos = strrpos($serverName, ':');
            if ($pos_2pontos !== false && ($pos_arroba === false || $pos_arroba < $pos_2pontos)) {
                $this->url->hierarchicalPart->authority->port->setValue(
                        substr($serverName, $pos_2pontos + 1)
                );
            } else {
                $pos_2pontos = false;
            }

            /* Se houver não houver userinfo e porta, tudo é host */
            if ($pos_arroba === false && $pos_2pontos === false) {
                $this->url->hierarchicalPart->authority->host->setValue(
                        $serverName
                );
            } else
            /* Se tiver userinfo e porta, o host é o que está no meio */
            if ($pos_arroba !== false && $pos_2pontos !== false) {
                $this->url->hierarchicalPart->authority->host->setValue(
                        substr($serverName, $pos_arroba + 1, $pos_2pontos - $pos_arroba - 1)
                );
            } else
            /* Se tiver apenas a porta, o host é o que está antes */
            if ($pos_2pontos !== false) {
                $this->url->hierarchicalPart->authority->host->setValue(
                        substr($serverName, 0, $pos_2pontos)
                );
            } else {
                $this->url->hierarchicalPart->authority->host->setValue(
                        substr($serverName, $pos_arroba + 1)
                );
            }
        }
    }

    /**
     * 
     * @param array $server
     */
    private function setHierarchicalFromServer(array $server)
    {
        $this->setHierarchicalAuthority($server['SERVER_NAME']);
        $this->setHierarchicalPath($server['REQUEST_URI']);
    }

    /**
     * 
     * @param string $string
     */
    private function setHierarchicalPath($string)
    {
        $pos_interrogacao = strpos($string, '?');
        if ($pos_interrogacao !== false) {
            $string = substr($string, 0, $pos_interrogacao);
        }
        $this->url->hierarchicalPart->path->setValue($string);
    }

    /**
     * 
     * @param array $server
     */
    private function setQueryFromServer(array $server)
    {
        if (isset($server['QUERY_STRING']) && !empty($server['QUERY_STRING'])) {
            $this->url->query->setValue($server['QUERY_STRING']);
        }
    }

    /**
     * 
     * @param array $server
     */
    private function setSchemeFromServer(array $server)
    {
        $this->url->scheme = (!isset($server['HTTPS']) || $server['HTTPS'] != "on") ?
                'http' :
                'https';
    }

}
